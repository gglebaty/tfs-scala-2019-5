package practice

import java.io.Closeable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.{BufferedSource, Source}
import scala.util.{Failure, Success}

case class Bucket(min: Int, max: Int, count: Int)

case class Stats(lineCount: Int,
                 minLineLength: Int,
                 averageLineLength: Int,
                 maxLineLength: Int,
                 buckets: Seq[Bucket])


object Practice extends App {
  private val bucketSize = 10
  private val defaultFileName = "in.txt"

  private def getSource(fileName: String): BufferedSource =
    Source.fromResource(fileName)
  private def read(in: BufferedSource): Future[Iterator[String]] =
    Future(blocking(in.getLines()))

  private def printStats(stats: Stats): Unit = {
    import stats._
    println(
      s"""
         | Total line count: $lineCount
         | min: $minLineLength
         | avg: $averageLineLength
         | max: $maxLineLength
         |
         | buckets:
         |${
        buckets.map { b =>
          import b._
          s"   - $min-$max: $count"
        }.mkString("\n")
      }""".stripMargin)
  }

  /*
    Методы для разминки
   */

  def asyncWithResource[R <: Closeable, T](resource: R)(block: R => Future[T]): Future[T] = {
    block(resource).andThen({case _ => resource.close()})
  }

  def asyncCountLines: Future[Int] = {
    asyncWithResource(getSource(defaultFileName)) (read(_).map(_.length))
  }

  def asyncLineLengths: Future[Seq[(String, Int)]] = {
    asyncWithResource(getSource(defaultFileName)) (read(_).map(_.toList)).map(it => it.map(s => (s, s.length)))
  }

  def asyncTotalLength: Future[Int] = {
    asyncWithResource(getSource(defaultFileName)) (read(_).map(_.toList)).map(it => it.map(s => s.length).sum)
  }

  def countShorterThan(maxLength: Int): Future[Int] = {
    asyncLineLengths.map(seq => seq.count(d => d._2 < maxLength))
  }


  /*
    Sleep sort
    https://www.quora.com/What-is-sleep-sort
   */

  def printWithDelay(delay: FiniteDuration, s: String) = {
    Future{ blocking({
      Thread.sleep(delay.toMillis)
      println(s"$s ${s.length}")
    })}
  }

  def sleepSort: Future[Unit] = {
    Future {
      val result = asyncLineLengths
      result.onComplete({
        case Success(value) =>
          Future.sequence(value.map(tuple => printWithDelay(new FiniteDuration(tuple._2 * 100, MILLISECONDS), tuple._1)))
        case Failure(exception) => exception.printStackTrace()
      })
    }

    asyncLineLengths.map(seq => seq.foreach(tuple => printWithDelay(new FiniteDuration(tuple._2 * 100, MILLISECONDS), tuple._1)))
  }

  /*
    Calculate file statistics
   */

  def splitToBuckets(linesWithLengths: Seq[(String, Int)]): Seq[Bucket] = {
    linesWithLengths.sortWith((d1,d2) => d1._2 < d2._2).grouped(bucketSize).map(seq => Bucket(seq.head._2, seq.last._2,seq.size)).toList
  }

  def calculateStats: Future[Stats] = {
    val lineCount = asyncCountLines
    val linesWithLength = asyncLineLengths
    val totalLength = asyncTotalLength

    for{
      lineCount <- lineCount
      totalLength <- totalLength
      linesWithLength <- linesWithLength
      buckets = splitToBuckets(linesWithLength)
    } yield Stats(lineCount, buckets.head.min, totalLength/lineCount, buckets.last.max, buckets)

  }

  val result = calculateStats
  result.onComplete({
    case Success(value) => printStats(value)
    case Failure(exception) => exception.printStackTrace()
  })

  val res = sleepSort

  Thread.sleep(7500)
}
