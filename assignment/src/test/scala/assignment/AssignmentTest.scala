package assignment

import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

import bcrypt.AsyncBcryptImpl
import com.typesafe.config.ConfigFactory
import org.scalatest.Matchers._
import org.scalatest._

import scala.concurrent.Future.never
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future, TimeoutException}

class AssignmentTest extends AsyncFlatSpec {

  val config = ConfigFactory.load()
  val credentialStore = new ConfigCredentialStore(config)
  val reliableBcrypt = new AsyncBcryptImpl
  val assignment = new Assignment(reliableBcrypt, credentialStore)(ExecutionContext.global)

  import assignment._

  behavior of "verifyCredentials"

  it should "return true for valid user-password pair" in {
    verifyCredentials("winnie", "pooh").map { result =>
      result shouldBe true
    }
  }

  it should "return false if user does not exist in store" in {
    verifyCredentials("pigle","pooh").map { result =>
      result shouldBe false

    }
  }
  it should "return false for invalid password" in {
    verifyCredentials("poodle", "dog").map( result =>
      result shouldBe false)
  }

  behavior of "withCredentials"

  def f:()=> String = () => "test"

  it should "execute code block if credentials are valid" in {
    withCredentials("winnie", "pooh")(f).map( result =>
      result shouldBe f
    )
  }
  it should "not execute code block if credentials are not valid" in {
    recoverToSucceededIf[InvalidCredentialsException] {
      withCredentials("puddle", "dog")(f)
    }
  }

  behavior of "hashPasswordList"

  it should "return matching password-hash pairs" in {
    hashPasswordList(Seq("123","test")).flatMap(seq =>
      Future.sequence(seq.map(tuple =>
        reliableBcrypt.verify(tuple._1, tuple._2)))).map(_ shouldBe Seq(true, true))
  }

  behavior of "findMatchingPassword"

  it should "return matching password from the list" in {
    val passwords = Seq("pooh","doge","test")
    val hash = reliableBcrypt.hash("test")
    hash.flatMap(h => findMatchingPassword(passwords, h).map(result => result shouldBe Some("test")))

  }
  it should "return None if no matching password is found" in {
    val passwords = Seq("pooh","doge","test")
    val hash = reliableBcrypt.hash("test2")
    hash.flatMap(h => findMatchingPassword(passwords, h).map(result => result shouldBe None))
  }

  behavior of "withRetry"

  it should "return result on passed future's success" in {
    val f = Future.successful("test")
    withRetry(f, 1).map(result => result shouldBe "test")
  }

  it should "not execute more than specified number of retries" in {
    val n = new AtomicInteger()
    def f() = Future {
      n.incrementAndGet()
      throw new IllegalArgumentException("test exception")
    }
    val retries = 5
    recoverToSucceededIf[IllegalArgumentException] {
      withRetry(f(), retries).map(_ => n.get() shouldBe retries)
    }

  }
  it should "not execute unnecessary retries" in {
    val n = new AtomicInteger()
    def f() = Future {
      n.incrementAndGet()
      if(n.get() < 5)
        throw new IllegalArgumentException("test exception")
    }
    withRetry(f(), 10).map(_ => n.get() shouldBe 5)
  }
  it should "return the first error, if all attempts fail" in {
    val n = new AtomicInteger()
    def f() = Future {
      n.incrementAndGet()
      if(n.get() == 1)
        throw new IllegalAccessException("test exception")
      else
        throw new IllegalArgumentException("test exception")
    }
    val retries = 5
    recoverToSucceededIf[IllegalAccessException] {
      withRetry(f(), retries)
    }
  }

  behavior of "withTimeout"

  it should "return result on passed future success" in {
    val f = Future.successful("test")
    withTimeout(f, FiniteDuration(10, TimeUnit.MILLISECONDS)).map(result => result shouldBe "test")
  }
  it should "return result on passed future failure" in {
    val f = Future.failed(new IllegalArgumentException)
    recoverToSucceededIf[IllegalArgumentException] {
      withTimeout(f, FiniteDuration(10, TimeUnit.MILLISECONDS))
    }
  }
  it should "complete on never-completing future" in {
    recoverToSucceededIf[TimeoutException] {
      withTimeout(never, FiniteDuration(10, TimeUnit.MILLISECONDS))
    }
  }

  behavior of "hashPasswordListReliably"
  val assignmentFlaky = new Assignment(new FlakyBcryptWrapper(reliableBcrypt), credentialStore)(ExecutionContext.global)

  it should "return password-hash pairs for successful hashing operations" in {
    hashPasswordListReliably(Seq("pooh","doge","test"),2,FiniteDuration(10,TimeUnit.MILLISECONDS)).flatMap(seq =>
      Future.sequence(seq.map(tuple =>
        reliableBcrypt.verify(tuple._1, tuple._2)))).map(seq => {
      seq.contains(false) shouldBe false
    })
    }

}
