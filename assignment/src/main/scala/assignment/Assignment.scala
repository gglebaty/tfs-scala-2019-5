package assignment

import java.time.LocalDateTime
import java.util.concurrent.TimeoutException

import bcrypt.AsyncBcrypt
import com.typesafe.scalalogging.StrictLogging
import store.AsyncCredentialStore
import util.Scheduler

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success}

class Assignment(bcrypt: AsyncBcrypt, credentialStore: AsyncCredentialStore)
                (implicit executionContext: ExecutionContext) extends StrictLogging {

  /**
    * проверяет пароль для пользователя
    * возвращает Future со значением false:
    *   - если пользователь не найден
    *   - если пароль не подходит к хешу
    */
  def verifyCredentials(user: String, password: String): Future[Boolean] =
    credentialStore.find(user).flatMap{
      case Some(hash)  => bcrypt.verify(password, hash)
      case _ => Future{false}
    }


  /**
    * выполняет блок кода, только если переданы верные учетные данные
    * возвращает Future c ошибкой InvalidCredentialsException, если проверка не пройдена
    */
  def withCredentials[A](user: String, password: String)(block: => A): Future[A] =
    verifyCredentials(user, password).flatMap {
      case true => Future{block}
      case _ => Future.failed(new InvalidCredentialsException)
    }

  /**
    * хеширует каждый пароль из списка и возвращает пары пароль-хеш
    */
  def hashPasswordList(passwords: Seq[String]): Future[Seq[(String, String)]] =
    Future.sequence(passwords.map(password => bcrypt.hash(password).map(hash => (password, hash))))

  /**
    * проверяет все пароли из списка против хеша, и если есть подходящий - возвращает его
    * если подходит несколько - возвращается любой
    */
  def findMatchingPassword(passwords: Seq[String], hash: String): Future[Option[String]] =
    Future.sequence(passwords.map(bcrypt.verify(_, hash))).map{
      bs => (passwords zip bs).filter(_._2).map(_._1)
    }.map(seq => seq.headOption)

  /**
    * логирует начало и окончание выполнения Future, и продолжительность выполнения
    */
  def withLogging[A](tag: String)(f: => Future[A]): Future[A] = {
    val start = System.currentTimeMillis()
    logger.trace(s"Begin task $tag at ${LocalDateTime.now}")
    val result = f
    result.onComplete {
      _ => val end = System.currentTimeMillis()
        logger.trace(s"End task $tag at ${LocalDateTime.now} in ${end - start} ms")
    }
    result
  }

  /**
    * пытается повторно выполнить f retries раз, до первого успеха
    * если все попытки провалены, возвращает первую ошибку
    *
    * Важно: f не должна выполняться большее число раз, чем необходимо
    */
  def withRetry[A](f: => Future[A], retries: Int): Future[A] = {
    f.recoverWith {
      case exception if retries > 0 => withRetry(f, retries - 1).recoverWith{
        case _ => throw exception
      }
    }
  }

  /**
    * по истечению таймаута возвращает Future.failed с java.util.concurrent.TimeoutException
    */
  def withTimeout[A](f: Future[A], timeout: FiniteDuration): Future[A] = {
    val promise = Promise[A]
    f.map(value => promise.success(value)).recover({
      case exception => promise.failure(exception)
    })
    Scheduler.executeAfter(timeout) {
      promise.failure(new TimeoutException)
    }
    promise.future
  }


  /**
    * делает то же, что и hashPasswordList, но дополнительно:
    *   - каждая попытка хеширования отдельного пароля выполняется с таймаутом
    *   - при ошибке хеширования отдельного пароля, попытка повторяется в пределах retries (свой на каждый пароль)
    *   - возвращаются все успешные результаты
    */
  def hashPasswordListReliably(passwords: Seq[String], retries: Int, timeout: FiniteDuration): Future[Seq[(String, String)]] = {
    Future.sequence(passwords.map(password =>
      withRetry(withTimeout(bcrypt.hash(password), timeout), retries)).map(future =>
      future.map(Success(_)).recover({
      case fail => Failure(fail)
    }))).map(passwords zip _).map(_.collect { case (password, Success(hash)) => (password, hash)})

  }
}
